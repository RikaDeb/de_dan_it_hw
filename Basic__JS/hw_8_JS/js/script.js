// ТЕОРІЯ-----------------------
//
//
// 1. Опишіть своїми словами як працює метод forEach.
//// Він виконує задану задачу один раз для кожного елементу масиву
//
// 2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.
//// Мутують: push(), pop(), unshift(), shift(), splice(), reverse(), sort(), fill(), copyWithin(). Повертають новий: concat(), slice(), map(), filter(), flatMap()
//
// 3. Як можна перевірити, що та чи інша змінна є масивом?
//// За допомогою функції isArray() та оператора instanceof
//
// 4. В яких випадках краще використовувати метод map(), а в яких forEach()?
//// Метод map() потрібен для створення нових масивів, а метод forEach() просто проходиться по елементах заданого масиву не створюючи новий
//
//
//ПРАКТИКА---------------------
"use strict";
//ЗАВДАННЯ №1////////////////////////////////////////////////////////
let arr = ["travel", "hello", "eat", "ski", "lift"];

let result = arr.filter((arr) => arr.length > 3)

console.log(result)

//ЗАВДАННЯ №2////////////////////////////////////////////////////////
// let users = [
//     { name: "Іван", age: 25, sex: "чоловіча" },
//     { name: "Марія", age: 21, sex: "жіноча" },
//     { name: "Руслана", age: 23, sex: "жіноча" },
//     { name: "Дмитро", age: 25, sex: "чоловіча" }
// ]
//
// let male = users.map(function (users) {
//     if (users.sex === "чоловіча") {
//         return users.sex;
//     }
//
// });
//
// console.log(male)

//ЗАВДАННЯ №3////////////////////////////////////////////////////////
// let arr = ["opop", 67, "84", { price: 567 }, null, undefined]
// let type = "undefined";
// let item;
//
// function filterBy(arr, type) {
//     return arr.filter(function (item) {
//         return typeof item !== type
//     })
// }
//
// console.log(filterBy(arr, type))