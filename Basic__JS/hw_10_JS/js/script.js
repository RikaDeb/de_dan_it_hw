// ТЕОРІЯ-----------------------
//
//
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
//// Можна використати document.createElement(tag), document.createTextNode(text) або elem.insertAdjacentHTML/Text/Element(where, html/text/elem). Перший створює вузол вказанго тега, другий створює текстовий вузол із заданим текстом, а третій може вставити елемент відносно іншого елемента.
//
// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
//// Припустимо у нас в коді є декілька різних елементів з класом "navigation". Для того щоб видалити його з усієї сторінки, треба пройтись циклом for..of по всіх елементах, що мають цей клас і в тілі циклу написати [element].removeAttribute("class"), тільки колекцію, яка нам повертається треба перетворити на масив.
//
// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
//// node.append()/prepend()/before()/after(). Також є параметри використовувані в методі elem.insertAdjacentHTML такі як: beforebegin, afterbegin, beforeend, afterend.
//
//
//ПРАКТИКА---------------------
"use strict";
//ЗАВДАННЯ №1////////////////////////////////////////////////////////
let link = document.createElement("a");
link.innerText = "Learn More";
link.setAttribute("href", "#");
document.getElementById("footer").append(link);

//ЗАВДАННЯ №2////////////////////////////////////////////////////////
// let select = document.createElement("select");
// select.setAttribute("id", "rating");
// main.prepend(select);
//
// for (let i = 0; i < 4; i++) {
//     let option = document.createElement("option");
//
//     option.innerText = (4 - i) + "Star";
//     option.setAttribute("value", (4 - i));
//
//     if (option.value > 1) {
//         option.innerText = (4 - i) + "Stars";
//     }
//     select.appendChild(option);
// }