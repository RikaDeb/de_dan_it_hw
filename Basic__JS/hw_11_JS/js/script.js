// ТЕОРІЯ-----------------------
//
//
// 1. Що таке події в JavaScript і для чого вони використовуються?
//// Це сигнал того, що сталося. Вони використовуються для маніпуляцій із сайтом.
//
// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
//// Сlick, contextmenu, mouseover, mouseout, mousedown, mouseup, mousemove.
//
// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
//// Подія contextmenu відбувається, коли користувач клікає на елемент правою кнопкою миші. Ця подія дає можливість створювати власні контекстні меню для сайтів і керувати їх поведінкою.
//
//
//ПРАКТИКА---------------------
"use strict";
//ЗАВДАННЯ №1////////////////////////////////////////////////////////
let button = document.getElementById("btn-click");

function createParagraph() {
    let paragraph = document.createElement("p");
    paragraph.innerText = "New Paragraph";
    document.getElementById("content").append(paragraph);
}

button.addEventListener("click", createParagraph);

//ЗАВДАННЯ №2////////////////////////////////////////////////////////
// let buttonInputCreate = document.createElement("button");
// buttonInputCreate.setAttribute("id", "btn-input-create");
// let section = document.getElementById("content");
// section.append(buttonInputCreate);
//
// function createInput() {
//     let newInput = document.createElement("input");
//     newInput.setAttribute("placeholder", "якийсь текст");
//     newInput.setAttribute("type", "password");
//     newInput.setAttribute("name", "Hi");
//     section.append(newInput)
// }
//
// buttonInputCreate.addEventListener("click", createInput);