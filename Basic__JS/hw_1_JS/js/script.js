// ТЕОРІЯ-----------------------
//
//
// 1. Як можна оголосити змінну у Javascript?
//// let або const.
//
// 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?
//// Рядок string це послідовність символів Юнікоду. Створити його можна за допомогою змінної let/const str = "Some string", або let/const str = new String("Some string").
//
// 3. Як перевірити тип даних змінної в JavaScript?
//// За допомогою оператора typeof.
//
// 4. Поясніть чому '1' + 1 = 11.
//// Бо коли оператор + зустрічає операнду рядок, то і іншу він теж перетворює на рядок і склеює їх.

// ПРАКТИКА---------------------
"use strict";

let randomNum = 13;
console.log(typeof randomNum);
//
//
const myName = "Erika";
const myLastName = "Debela";
console.log(`Мене звати ${myName}, ${myLastName}`);
//
//
let num = 89;
let strNum = String(num);
console.log(strNum);
