// ТЕОРІЯ-----------------------
//
//
// 1. Як можна створити рядок у JavaScript?
//// За допомогою лапок
//
// 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
//// Одинарні та подвійні лапки використовуються одні в одних, коли в рядку треба окремий текст поставити в лапки. Зворотні відрізняються тим, що в них може бути записаний як і текст так і змінні, можна записувати тест на декілткох рядках
//
// 3. Як перевірити, чи два рядки рівні між собою?
//// Через операторb порівння, результатом буде значення boolean
//
// 4. Що повертає Date.now()?
//// Кілікість мілісекунл, що пройшли з 01.01.1970 до поточного часу
//
// 5. Чим відрізняється Date.now() від new Date()?
//// Date.now() повертає кілікість мілісекунл, що пройшли з 01.01.1970 до поточного часу, а new Date() повертає об'єкт в якому записана точна дата коли цей об'ґкт було виуликано
//
//ПРАКТИКА---------------------
"use strict";
//ЗАВДАННЯ №1////////////////////////////////////////////////////////
let str = prompt("Введіть слово");

let isPalindrome = function (str) {
  if (str === str.split("").reverse().join("")) {
    return true;
  } else {
    return false;
  }
};

// console.log(isPalindrome(str));

//ЗАВДАННЯ №2////////////////////////////////////////////////////////
// let str = prompt("Введіть рядок");
// let maxStringLength = Number(8);
//
// let checkStringLength = function (str, maxStringLength) {
//   if (str.length <= maxStringLength) {
//     return true;
//   } else {
//     return false;
//   }
// };
//
// console.log(checkStringLength(str, maxStringLength));

//ЗАВДАННЯ №3////////////////////////////////////////////////////////
// let birthDate = new Date(prompt("Введіть свою дату народження: yyyy-mm-dd"));
// let nowDate = new Date();
//
// let checkBirthDate = function (birthDate, nowDate) {
//   let calcAge = nowDate - birthDate;
//
//   let ageDate = new Date(calcAge);
//   let age = Math.abs(ageDate.getUTCFullYear() - 1970);
//
//   return age;
// };
//
// console.log(checkBirthDate(birthDate, nowDate));
