// ТЕОРІЯ-----------------------
//
//
// 1. Опишіть своїми словами, що таке метод об'єкту
//// Властивість стає методом, якщо значення ключа це функція
//
// 2. Який тип даних може мати значення властивості об'єкта?
//// Будь-який
//
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
//// Це внутрішній тип, який використовують для передачі інформації від крапки до дужок.

//ПРАКТИКА---------------------
"use strict";

let product = {
  name: "iPad",
  price: 2568,
  discount: 50,
};

(product.fullPrice = function () {
  if (product.discount == 50) {
    product.price = product.price / 2;
    return product.price;
  }
}),
  console.log(product.fullPrice());
//
//
//
// let enterName = prompt("Введіть своє ім'я");
// let enterAge = prompt("Введіть свій вік");

// let person = {
//   name: enterName,
//   age: enterAge,
// };

// function getObj(person) {
//   return `Привіт, я ${enterName}, мені ${enterAge} рік(и)(ів)`;
// }

// alert(getObj(person));
