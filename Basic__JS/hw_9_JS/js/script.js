// ТЕОРІЯ-----------------------
//
//
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
//// Це те що допомогає javascript маніпулювати структурою, стилями та вмістом сайту, щоб створювати динамічні вебсторінки.
//
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//// innerHTML дозволяє змінювати наповенення html елементу такі як класи, id, атрибути, тощо, але з нею треба бути дуже обережними бо вона розбирає html і через це є вирогідність отримати кібер атаку на сайт.
//// innerText може лише змінювати або отримувати текст всередині елемнта, ігноруючи будь-які html теги. Замінюючи текст вона також враховує його стилі.
//
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//// Для цього можна використати або getElementById/Class/TagName/Name(), або querySelector/All(). Неможна сказати, що один метод кращий за інший так як використовуються вони для різних цілей: 
//// Методи типу getElement() надають прямий доступ до елементів і тим самим будуть швидшими за методи типу querySelector().
//// В свою чергу методи типу querySelector() дозволяють обирати елементи через більш складні селектори, та через це швидкість їх обробки може зайняти трохи більш чаму ніж у методів типу getElement().
//
// 4. Яка різниця між nodeList та HTMLCollection?
//// Обидві HTMLCollection та nodeList є колекціями, але nodeList повертається від методів типу querySelector() і не є живою, а HTMLCollection є живою і повертається від методів типу getElement().
//
//
//ПРАКТИКА---------------------
"use strict";
//ЗАВДАННЯ №1////////////////////////////////////////////////////////
//let myFeatureElements = document.querySelectorAll("p"); //--- Перший спосіб знаходження елементу
let myFeatureElements = document.getElementsByClassName("feature"); //--- Другий спосіб знаходження елементу
console.log(myFeatureElements);

for (let e of myFeatureElements) {
    e.style.textAlign = "center";
}

//ЗАВДАННЯ №2////////////////////////////////////////////////////////
//let newText = document.querySelectorAll("h2");
//
//for (let i of newText) {
//    i.textContent = "Awesome feature";
//}

//ЗАВДАННЯ №3////////////////////////////////////////////////////////
//let exclamationMarkAdd = document.getElementsByClassName("feature-title");
//
//for (let j of exclamationMarkAdd) {
//    j.innerText = `${j.innerText}!`;
//}