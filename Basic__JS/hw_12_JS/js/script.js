// ТЕОРІЯ-----------------------
//
//
// 1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
//// Через властивості event.key або event.code.
//
// 2. Яка різниця між event.code() та event.key()?
//// Властивіть code доозволяє отримати фізичний код клавіші, а key властивість. Тобто для event.code "a" та "A" це одне і те ж саме, а для event.key це два різних значення.
//
// 3. Які три події клавіатури існує та яка між ними відмінність?
//// Keydown, keyup та keypress. Перша виникає коли клавіша нажата, друга коли її відпустили, а третю в сучасній розробці не використовують.
//
//
//ПРАКТИКА---------------------
"use strict";
let keys = document.querySelectorAll(".btn");

window.addEventListener("keydown", function (e) {
    keys.forEach(function (keys) {
        if (e.key == keys.innerText) {
            keys.style.backgroundColor = "blue";
        } else {
            keys.style.backgroundColor = "black";
        }
    })
})