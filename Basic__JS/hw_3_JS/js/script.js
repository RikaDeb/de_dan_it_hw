// ТЕОРІЯ-----------------------
//
//
// 1. Що таке логічний оператор?
//// Це оператор, який використовується для виконання операцій на логічних значеннях(зазвичай  true або false).
//
// 2. Які логічні оператори є в JavaScript і які їх символи?
//// Є оператор "і" із символом "&&", "або" із символом "||" та "ні" із символом "!"

//ПРАКТИКА---------------------
"use strict";

let age = prompt("Введіть свій вік:");

if (age <= 12) {
  alert("Ви дитина");
} else if (age < 18) {
  alert("Ви підліток");
} else if (age >= 18) {
  alert("Ви доросла");
}
//
let month = prompt("Введіть місяць року:");
let lowerCaseTransform = month.toLocaleLowerCase();

switch (lowerCaseTransform) {
  case "січень":
    console.log("У цьому місяці 31 день");
    break;
  case "лютий":
    console.log("Це високосний рік, тож у цьому році у цьому місяці 29 днів");
    break;
  case "березень":
    console.log("У цьому місяці 31 день");
    break;
  case "квітень":
    console.log("У цьому місяці 30 днів");
    break;
  case "травень":
    console.log("У цьому місяці 31 день");
    break;
  case "червень":
    console.log("У цьому місяці  30 днів");
    break;
  case "липень":
    console.log("У цьому місяці 31 день");
    break;
  case "серпень":
    console.log("У цьому місяці 31 день ");
    break;
  case "вересень":
    console.log("У цьому місяці 30 днів");
    break;
  case "жовтень":
    console.log("У цьому місяці 31 день");
    break;
  case "листопад":
    console.log("У цьому місяці 30 днів");
    break;
  case "грудень":
    console.log("У цьому місяці 31 день");
    break;
  default:
    break;
}
